import React, { Component } from 'react'
import Menu from "../Menu/Menu";
import {login, fetchAllStudents} from "../Common/Api"
import {MENU_ITEMS,username,password} from "../Common/Constants"
import StudentsList from '../Students/StudentsList'
export default class Home extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
             token:"",
             selectedOption:0,
             students:[]
        }
    }
     async componentDidMount(){
        let token = await login(username,password)
        this.setState({token:token})
    }
    handleMenuClick = async (event)=>{
        switch (event.target.name) {
            //obraditi sve ostale događaje
            case "Students":
                let students = await fetchAllStudents(this.state.token)
                this.setState({students:students, selectedOption:2})
                break;
            default:
                break;
        }
    }
    render() {
        return (
            <div>
                <Menu items={MENU_ITEMS} onMenuClick = {this.handleMenuClick}/>
                {this.state.selectedOption===2 && <StudentsList students={this.state.students}/>}
            </div>
        )
    }
}
