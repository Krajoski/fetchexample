import {URL} from "./Constants.js"
export async function fetchAllStudents(token) {
    let userData = await fetch(`${URL}/api/students`, {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            Authorization: 'Bearer ' + token,
        },
    })
    return userData.json();
}

export async function login(username, password) {
    try {
        let url = `${URL}/token`
        let tockenPromise = await fetch(url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            body: `grant_type=password&username=${username}&password=${password}`
        })
        let token = await tockenPromise.json();
        let accessToken = token.access_token;
        if (accessToken) {
            alert(accessToken)
            return accessToken
        }
        else {
            return "";
        }
    } catch (error) {
        alert("ERROR!!!")
    }

}
