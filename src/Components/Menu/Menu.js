import React, { Component } from 'react'

export default class DashboardMenu extends Component {
    render() {
        const {items, onMenuClick} = this.props
        const menuItems = items.map(item =><button type="button" key={item} name={item} className="btn btn-secondary" onClick={onMenuClick}>{item}</button>)
        return (
            <div>
                <div className="btn-group btn-group-lg" role="group">
                {menuItems}
                </div>
            </div>
        )
    }
}
