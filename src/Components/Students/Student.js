import React, { Component } from 'react'

export default class Student extends Component {
    render() {
        return (
        <tr>
                <td>{this.props.id}</td>
                <td>{this.props.student.firstName}</td>
                <td>{this.props.student.lastName}</td>
                <td>{this.props.student.gender}</td>
                <td>{this.props.student.phone}</td>
                <td>{this.props.student.email}</td>
                <td>{this.props.student.placeOfBirth}</td>
                <td>{this.props.student.userName}</td>   
                <td>{this.props.student.classRoom}</td>
                <td>{this.props.student.classRoomId}</td>
                <td><button id={this.props.id}  onClick={this.props.delete}>Delete</button></td>
                </tr>           
                

        )
    }
}
