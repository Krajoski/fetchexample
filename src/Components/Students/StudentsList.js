import React, { Component } from 'react'
import Student from './Student';

export default class StudentsList extends Component {
    delete = (event)=>{
        //implementirati funkciju za brisanje studenta iz liste
        alert(JSON.stringify(this.props.students[event.target.id]))
    }
    render() {
        const students = this.props.students.map((item,index) => <Student student={item} id={index} delete={this.delete} /> )
        return (
            <div>
                <table className="table">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">First name</th>
                            <th scope="col">Last name</th>
                            <th scope="col">Gender</th>
                            <th scope="col">Phone</th>
                            <th scope="col">Email</th>  
                            <th scope="col">Username</th>
                            <th scope="col">Place Of Birth</th>
                            <th scope="col">Classroom</th>
                            <th scope="col">Classroom ID</th>
                            <th scope="col"></th>
                        </tr>
                    </thead>
                    <tbody>
                    {students}
                    </tbody>
                </table>
            </div>
        )
    }
}
